/* Exercise 1

Class A {}

    Class B extends A{
        public D d = new D();
        public List<C> cList;
        public List<E> eList;

        private String param;

	public B(List<E> eList) {
            cList = new ArrayList<>();
            this.eList = eList;
        }

        public void x();
        public void y();

    }

    Class C {}
    Class E {}
    Class U {
        B b;
    }*/

public class ThreadApp extends Thread {
    public ThreadApp(String name) {
        super(name);
    }

    public static void main(String[] args) {
        ThreadApp myThread1 = new ThreadApp("MyThread1");
        ThreadApp myThread2 = new ThreadApp("MyThread2");
        ThreadApp myThread3 = new ThreadApp("MyThread3");


        /**
         * in cazut metodei run, fiecare counter numara pe rand
         * in timp ce se asteapta o secunda
         * pana sa se printeze urmatoarea valoare
         */
        myThread1.run();
        myThread2.run();
        myThread3.run();
    }

    public void run() {
        for (int i = 1; i <= 10; i++) {
            System.out.println(getName() + " message number  = " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
}

